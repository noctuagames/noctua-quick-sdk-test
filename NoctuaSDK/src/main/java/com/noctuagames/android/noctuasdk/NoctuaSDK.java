package com.noctuagames.android.noctuasdk;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.noctuagames.android.noctuasdk.activity.NoctuaWebViewActivity;
import com.noctuagames.android.noctuasdk.database.NoctuaDBClient;
import com.noctuagames.android.noctuasdk.database.entity.Quest;
import com.quickgame.android.sdk.QuickGameManager;
import com.quickgame.android.sdk.bean.QGUserBindInfo;
import com.quickgame.android.sdk.bean.QGUserData;
import com.quickgame.android.sdk.constans.QGConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NoctuaSDK {
    private final static String BASE_URL = "https://api-gtw.noctua.gg/";
    private final static String QUEST_WEBVIEW = "quest/v1/public/quest/claim";
    private final static String REWARD_WEBVIEW = "shop/v1/public/shops/exchange";
    private final static String REPORT_QUEST = "quest/v1/public/quest/event";

    private final static String TAG = "NoctuaSDK";
    private final static int MY_SOCKET_TIMEOUT_MS = 10000;
    private final static int MAX_RETRY = 10;

    private final static int OPEN_CLAIM_QUEST_REQUEST_CODE = 200001;
    private final static int OPEN_REDEEM_REWARD_REQUEST_CODE = 200002;
    private final static int OPEN_URL_REQUEST_CODE = 200999;

    private static NoctuaSDK mInstance;
    private static String mProductID;
    private static QuickGameManager mSdkInstance;
    private static Activity mActivity;
    private RequestQueue mRequestQueue;

    private NoctuaSDK(Activity activity, String mProductID, QuickGameManager mSdkInstance) {
        this.mActivity = activity;
        this.mProductID = mProductID;
        this.mSdkInstance = mSdkInstance;
        this.mRequestQueue = Volley.newRequestQueue(activity);
        VolleyLog.DEBUG = true;

        this.resendQuestEvents();
    }

    public static synchronized NoctuaSDK getInstance(Activity activity, String productID, QuickGameManager sdkInstance) {
        if (mInstance == null) {
            mInstance = new NoctuaSDK(activity, productID, sdkInstance);
        }
        return mInstance;
    }

    public void reportQuest(String eventName, double eventValue) {
        try {
            int userID = Integer.parseInt(mSdkInstance.getUser().getUid());
            long unixTime = System.currentTimeMillis() / 1000L;

            saveQuest(userID, unixTime, eventName, eventValue);
            logQuest(eventName, eventValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void logQuest(String eventName, double eventValue) {
        // Appsflyer Log
        Map<String, Object> eventValues = new HashMap<>();
        eventValues.put("quest_value", eventValue);
        mSdkInstance.appsFlyerEvent(eventName, eventValues);

        // Facebook Log
        mSdkInstance.logEvent(eventName, eventValue);

        // Firebase Log
        Bundle bundle = new Bundle();
        bundle.putDouble("quest_value", eventValue);
        mSdkInstance.getFirebaseManager(mActivity).logCustomEvent(eventName, bundle);
    }

    private void resendQuestEvents() {
        class ResendAllQuests extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                List<Quest> allQuests = NoctuaDBClient.getInstance(mActivity).getNoctuaDatabase()
                        .questDao()
                        .getAll();

                Log.d(TAG, "There are " + allQuests.size() + " quest(s) in DB. Resend one by one...");

                for (Quest quest : allQuests) {
                    NoctuaSDK.this.sendQuestEvent(quest.getId(), quest.getUserId(), quest.getTimestamp(), quest.getEventName(), quest.getEventValues());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }

        ResendAllQuests rsq = new ResendAllQuests();
        rsq.execute();
    }

    private void saveQuest(final int uid, final long ts, final String name, final double value) {
        class SaveQuest extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                //creating a quest
                Quest quest = new Quest();
                quest.setUserId(uid);
                quest.setEventName(name);
                quest.setEventValues(value);
                quest.setTimestamp(ts);

                //adding to database
                long id = NoctuaDBClient.getInstance(mActivity).getNoctuaDatabase()
                        .questDao()
                        .insert(quest);

                Log.d(TAG, "Quest is saved. ID: " + id);

                NoctuaSDK.this.sendQuestEvent(id, uid, ts, name, value);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }

        SaveQuest sq = new SaveQuest();
        sq.execute();
    }

    private void deleteQuest(final long questId) {
        class DeleteQuest extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                NoctuaDBClient.getInstance(mActivity).getNoctuaDatabase()
                        .questDao()
                        .delete(questId);

                Log.d(TAG, "Quest is deleted. ID: " + questId);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }

        DeleteQuest dq = new DeleteQuest();
        dq.execute();
    }

    private boolean sendQuestEvent(final long questId, int userID, long timestamp, String eventName, double eventValue) {
        try {
            String tokenInput = "eventCode=" + eventName + "&ts=" + timestamp + "&uid=" + userID + "&" + mProductID;
            String tokenHash = MD5Utils.hashMD5(tokenInput);
            String uri = BASE_URL + REPORT_QUEST;
            Log.d(TAG, "URI: " + uri);

            JSONObject params = new JSONObject();
            params.put("productCode", mProductID);

            JSONObject userParams = new JSONObject();
            userParams.put("uid", userID);
            params.put("userData", userParams);

            JSONObject eventParams = new JSONObject();
            eventParams.put("name", eventName);
            eventParams.put("value", eventValue);
            params.put("eventData", eventParams);

            params.put("ts", timestamp);
            params.put("sign", tokenHash);

            Log.d(TAG, params.toString(2));

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, uri, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                VolleyLog.v("Response:%n %s", response.toString(4));
                                deleteQuest(questId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.e("Error: ", error.getMessage());
                }
            }
            );

            // set retry policy
            req.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    MAX_RETRY,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            // add the request object to the queue to be executed
            this.addToRequestQueue(req);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void openClaimQuest(final Activity activity) {
        try {
            String userID = mSdkInstance.getUser().getUid();
            long unixTime = System.currentTimeMillis() / 1000L;
            String tokenInput = "userId=" + userID + "&ts=" + unixTime + "&" + mProductID;
            String tokenHash = MD5Utils.hashMD5(tokenInput);
            String uri = BASE_URL + QUEST_WEBVIEW + "?productCode=" + mProductID + "&ts=" + unixTime + "&userID=" + userID + "&token=" + tokenHash;
            Log.d(TAG, "URI: " + uri);

            logQuest("open_claim_quest_webview", 1);
            Intent webviewIntent = new Intent(activity, NoctuaWebViewActivity.class);
            webviewIntent.putExtra("url", uri);
            activity.startActivityForResult(webviewIntent, OPEN_CLAIM_QUEST_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openRedeemReward(final Activity activity) {
        openRedeemReward(activity, "", "");
    }

    public void openRedeemReward(final Activity activity, String roleId, String serverId) {
        try {
            String userID = mSdkInstance.getUser().getUid();
            long unixTime = System.currentTimeMillis() / 1000L;
            String tokenInput = "userId=" + userID + "&ts=" + unixTime + "&" + mProductID;
            String tokenHash = MD5Utils.hashMD5(tokenInput);
            String uri = BASE_URL + REWARD_WEBVIEW + "?productCode=" + mProductID + "&ts=" + unixTime + "&userID=" + userID + "&token=" + tokenHash + "&accountID=" + roleId + "&serverID=" + serverId;
            Log.d(TAG, "URI: " + uri);

            logQuest("open_redeem_reward_webview", 1);
            Intent webviewIntent = new Intent(activity, NoctuaWebViewActivity.class);
            webviewIntent.putExtra("url", uri);
            webviewIntent.putExtra("roleId", roleId);
            webviewIntent.putExtra("serverId", serverId);
            activity.startActivityForResult(webviewIntent, OPEN_REDEEM_REWARD_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openURL(final Activity activity, final String uri) {
        try {
            Log.d(TAG, "openURL - URI: " + uri);

            Intent webviewIntent = new Intent(activity, NoctuaWebViewActivity.class);
            webviewIntent.putExtra("url", uri);
            activity.startActivityForResult(webviewIntent, OPEN_URL_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult, requestCode: " + requestCode + ", resultCode: " + resultCode);
        if(requestCode == NoctuaSDK.OPEN_REDEEM_REWARD_REQUEST_CODE
                || requestCode == NoctuaSDK.OPEN_CLAIM_QUEST_REQUEST_CODE
                || requestCode == NoctuaSDK.OPEN_URL_REQUEST_CODE) {
            Log.d(TAG, "From redeem quest activity");
            if(resultCode == Activity.RESULT_OK) {
                Log.d(TAG, "Result OK. Data: " + data.getDataString());
                boolean doBindAccount = data.getBooleanExtra("do_bind_account", false);
                final String url = data.getStringExtra("current_url");
                final String roleId = data.getStringExtra("role_id");
                final String serverId = data.getStringExtra("server_id");
                if(doBindAccount) {
                    Log.d(TAG, "Request to bind account");
                    QGUserData info = mSdkInstance.getUser();
                    if(info.isGuest()) {
                        Log.d(TAG, "User is guest. Proceed to bind email.");
                        mSdkInstance.setUserBindCallback(new QuickGameManager.QGUserBindCallback() {
                            @Override
                            public void onBindInfoChanged(String uid, boolean isBindUnbindSuccess, QGUserBindInfo bindInfo) {
                                if(bindInfo.isBindEmail()) {
                                    Log.d(TAG, "bind email success");
                                } else {
                                    Log.d(TAG, "bind email fail");
                                }

                                if(requestCode == NoctuaSDK.OPEN_REDEEM_REWARD_REQUEST_CODE) {
                                    Log.d(TAG, "Opening Redeem Reward page back");
                                    openRedeemReward(mActivity, roleId, serverId);
                                } else if(requestCode == NoctuaSDK.OPEN_CLAIM_QUEST_REQUEST_CODE) {
                                    Log.d(TAG, "Opening Claim Quest page back");
                                    openClaimQuest(mActivity);
                                } else {
                                    Log.d(TAG, "Opening URL back: " + url);
                                    if(url != null) {
                                        openURL(mActivity, url);
                                    }
                                }
                            }

                            @Override
                            public void onexitUserCenter() {
                                Log.d(TAG, "Cancel user center");
                            }
                        });
                        mSdkInstance.bindUser(QGConstant.LOGIN_OPEN_TYPE_EMAIL);
                    } else {
                        Log.d(TAG, "User is binded. Re open redeem reward page");
                    }
                } else {
                    Log.d(TAG, "Not binding account request");
                }
            } else {
                Log.d(TAG, "Activity result is not OK");
            }
        }
    }

    private <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        mRequestQueue.add(req);
    }

    private <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        mRequestQueue.add(req);
    }

    private void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    static class MD5Utils {
        private static final Charset UTF_8 = Charset.forName("UTF-8");
        private static final String OUTPUT_FORMAT = "%-20s:%s";

        private static byte[] digest(byte[] input) {
            MessageDigest md;
            try {
                md = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                throw new IllegalArgumentException(e);
            }
            byte[] result = md.digest(input);
            return result;
        }

        private static String bytesToHex(byte[] bytes) {
            StringBuilder sb = new StringBuilder();
            for (byte b : bytes) {
                sb.append(String.format("%02x", b));
            }
            return sb.toString();
        }

        public static String hashMD5(String input) {
            byte[] md5InBytes = MD5Utils.digest(input.getBytes(UTF_8));
            String hex = bytesToHex(md5InBytes);
            int length = md5InBytes.length;
            System.out.println(String.format(OUTPUT_FORMAT, "Input (string)", input));
            System.out.println(String.format(OUTPUT_FORMAT, "Input (length)", input.length()));
            System.out.println(String.format(OUTPUT_FORMAT, "MD5 (hex) ", hex));
            System.out.println(String.format(OUTPUT_FORMAT, "MD5 (length)", length));

            return hex;
        }
    }
}
