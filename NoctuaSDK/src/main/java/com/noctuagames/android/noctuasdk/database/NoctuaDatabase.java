package com.noctuagames.android.noctuasdk.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.noctuagames.android.noctuasdk.database.dao.QuestDao;
import com.noctuagames.android.noctuasdk.database.entity.Quest;

@Database(entities = {Quest.class}, version = 1)
public abstract class NoctuaDatabase extends RoomDatabase {
    public abstract QuestDao questDao();
}
