package com.noctuagames.android.noctuasdk.database;

import android.content.Context;

import androidx.room.Room;

public class NoctuaDBClient {
    private Context mContext;
    private static NoctuaDBClient mInstance;

    private NoctuaDatabase mDatabase;

    private NoctuaDBClient(Context aContext) {
        this.mContext = aContext;

        mDatabase = Room.databaseBuilder(mContext, NoctuaDatabase.class, "Noctua").build();
    }

    public static synchronized NoctuaDBClient getInstance(Context aContext) {
        if(mInstance == null) {
            mInstance = new NoctuaDBClient(aContext);
        }

        return mInstance;
    }

    public NoctuaDatabase getNoctuaDatabase() {
        return mDatabase;
    }
}
