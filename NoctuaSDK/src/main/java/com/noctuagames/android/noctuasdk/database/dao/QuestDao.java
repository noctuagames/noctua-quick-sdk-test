package com.noctuagames.android.noctuasdk.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.noctuagames.android.noctuasdk.database.entity.Quest;

import java.util.List;

@Dao
public interface QuestDao {

    @Query("SELECT * FROM quest")
    List<Quest> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Quest quest);

    @Query("DELETE FROM quest WHERE id = :id")
    void delete(long id);

    @Update
    void update(Quest quest);
}
