package com.noctuagames.android.noctuasdk.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;

import com.noctuagames.android.noctuasdk.R;

public class NoctuaWebViewActivity extends Activity {
    private String TAG = getClass().getSimpleName();
    private String bindAccountKeyword = "bind-account-noctua";
    private String openNativeBrowserKeyword = "go-to-noctua-platform";

    private WebView webView = null;
    private TextView tv_title_web = null;
    private FrameLayout backFL = null;
    private String url;
    private String title;
    private String roleId;
    private String serverId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        setContentView(R.layout.noctua_webview);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        try {
            WindowInsetsControllerCompat wicc = WindowCompat.getInsetsController(getWindow(), getWindow().getDecorView());
            if(wicc != null) {
                wicc.setSystemBarsBehavior(WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
                wicc.hide(WindowInsetsCompat.Type.systemBars());
            }
        } catch (Throwable t) {
            t.printStackTrace();
            try {
                WindowInsetsControllerCompat wicc2 = ViewCompat.getWindowInsetsController(getWindow().getDecorView());
                if(wicc2 != null) {
                    wicc2.setSystemBarsBehavior(WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
                    wicc2.hide(WindowInsetsCompat.Type.systemBars());
                }
            } catch(Throwable t2) {
                t2.printStackTrace();
            }
        }

        init();
        url = getIntent().getStringExtra("url");
        title = getIntent().getStringExtra("title");
        roleId = getIntent().getStringExtra("roleId");
        serverId = getIntent().getStringExtra("serverId");

        if (!TextUtils.isEmpty(title)) {
            tv_title_web.setText("" + title);
        } else {
            tv_title_web.setText("");
        }

        if (TextUtils.isEmpty(url)) {
            Toast.makeText(this, "No internet connection!", Toast.LENGTH_LONG).show();
            finish();
        }

        loadUrl();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown");
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void loadUrl() {
        Log.d(TAG, "loadUrl");
        webView.loadUrl(url);
    }


    private void init() {
        Log.d(TAG, "init");
        backFL = findViewById(R.id.fl_back);
        webView = findViewById(R.id.wv_pay);
        tv_title_web = findViewById(R.id.tv_title_web);

        backFL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        webView.clearCache(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager csm = CookieSyncManager.createInstance(this);
            csm.startSync();
            CookieManager cm = CookieManager.getInstance();
            cm.removeAllCookie();
            cm.removeSessionCookie();
            csm.stopSync();
            csm.sync();
        }

        CookieManager.getInstance().setAcceptCookie(true);
        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        }
        WebSettings webSettings = webView.getSettings();
        webSettings.setDomStorageEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setDisplayZoomControls(false);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webSettings.setBlockNetworkImage(false);

        String ua = webSettings.getUserAgentString();
        webSettings.setUserAgentString(ua + "NoctuaSDKAndroid");
        webView.setWebViewClient(new MyWebViewClient());
    }


    class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.d(TAG, "onPageStarted::::" + url);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d(TAG, "shouldOverrideUrlLoading::::" + url);
            Log.d(TAG, "previous URL:" + view.getUrl());

            if(url.contains(bindAccountKeyword)) {
                Log.d(TAG, "URL contains keyword " + bindAccountKeyword + ". Close web view and open bind account.");
                Intent data = new Intent();
                data.putExtra("do_bind_account", true);
                data.putExtra("current_url", view.getUrl());
                data.putExtra("next_url", url);
                data.putExtra("role_id", roleId);
                data.putExtra("server_id", serverId);
                NoctuaWebViewActivity.this.setResult(Activity.RESULT_OK, data);
                NoctuaWebViewActivity.this.finish();
            } else if(url.contains(openNativeBrowserKeyword)) {
                Log.d(TAG, "URL contains keyword " + openNativeBrowserKeyword + ". Opening url with native browsser.");
                view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            }

            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.d(TAG, "onPageFinished::::" + url);
            if (url.startsWith("sms:")) {
                if (webView.canGoBack()) {
                    webView.goBack();
                }
            }
            super.onPageFinished(view, url);
        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            Log.d(TAG, "onReceivedSslError");
            final AlertDialog.Builder builder = new AlertDialog.Builder(NoctuaWebViewActivity.this);
            String message = "SSL Certificate error.";
            switch (error.getPrimaryError()) {
                case SslError.SSL_UNTRUSTED:
                    message = "The certificate authority is not trusted.";
                    break;
                case SslError.SSL_EXPIRED:
                    message = "The certificate has expired.";
                    break;
                case SslError.SSL_IDMISMATCH:
                    message = "The certificate Hostname mismatch.";
                    break;
                case SslError.SSL_NOTYETVALID:
                    message = "The certificate is not yet valid.";
                    break;
            }
            message += " Do you want to continue anyway?";

            builder.setTitle("SSL Certificate Error");
            builder.setMessage(message);
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
}
