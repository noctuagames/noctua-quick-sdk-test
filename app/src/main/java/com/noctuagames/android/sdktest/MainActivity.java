package com.noctuagames.android.sdktest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.appsflyer.AppsFlyerLib;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.Task;
import com.noctuagames.android.sdktest.R;
import com.google.firebase.messaging.RemoteMessage;
import com.noctuagames.android.noctuasdk.NoctuaSDK;
import com.quickgame.android.sdk.QuickGameManager;
import com.quickgame.android.sdk.QuickGameManager.QGPaymentCallback;
import com.quickgame.android.sdk.QuickGameManager.SDKCallback;
import com.quickgame.android.sdk.bean.QGOrderInfo;
import com.quickgame.android.sdk.bean.QGRoleInfo;
import com.quickgame.android.sdk.bean.QGUserBindInfo;
import com.quickgame.android.sdk.bean.QGUserData;
import com.quickgame.android.sdk.constans.QGConstant;
import com.quickgame.android.sdk.firebase.HWFirebaseCallback;
import com.quickgame.android.sdk.firebase.HWFirebaseManager;
import com.quickgame.android.sdk.model.QGUserHolder;
import com.quickgame.android.sdk.utils.log.QGLog;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private QuickGameManager sdkInstance;
    private NoctuaSDK noctuaSdk;
    private HWFirebaseManager firebaseManager;
    private Button loginBtn;
    private EditText edTotalFee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hw_activity_main);
        loginBtn = findViewById(R.id.login);
        edTotalFee = findViewById(R.id.txt_totalfee);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        WindowInsetsControllerCompat wicc = WindowCompat.getInsetsController(getWindow(), getWindow().getDecorView());
        if(wicc != null) {
            wicc.setSystemBarsBehavior(WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
            wicc.hide(WindowInsetsCompat.Type.systemBars());
        }

        //sdk init
        SampleSDKCallback sdkCallback = new SampleSDKCallback();
        sdkInstance = QuickGameManager.getInstance();
        // init by your product code
        sdkInstance.init(this, "82061201057614433444654615653722", sdkCallback);
        sdkInstance.onCreate(this);
        AppsFlyerLib.getInstance().setDebugLog(true);
        noctuaSdk = NoctuaSDK.getInstance(MainActivity.this, "82061201057614433444654615653722", sdkInstance);
        firebaseManager = QuickGameManager.getInstance().getFirebaseManager(this);
        FacebookSdk.setIsDebugEnabled(true);

        sdkInstance.setFirbMsgCallback(this,new HWFirebaseCallback() {
            @Override
            public void onGetToken(boolean isSuccess, String token) {
                Log.e("quickgame","registration token: " + isSuccess + " token: " + token);
                //isSuccess:true is Success，false is fail
                //token： on failure, the token is null
            }

            @Override
            public void onNewToken(String token) {
                Log.e("quickgame","newtoken: " + token);
                AppsFlyerLib.getInstance().updateServerUninstallToken(getApplicationContext(), token);
            }
            @Override
            public void onMessageReceived(RemoteMessage remoteMessage) {
                //A push message callback is received
                Log.e("quickgame","msg Body: " + remoteMessage.getNotification().getBody());
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                sdkInstance.getFirebaseManager(MainActivity.this).sendNotification(MainActivity.this, remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), intent);
            }
        });
    }

    // show login view
    public void callLogin(View v) {
        sdkInstance.login(this);
    }

    //show pay view
    public void callPayment(View v) {
        String totalFeeStr = edTotalFee.getText().toString();
        if (TextUtils.isEmpty(totalFeeStr)) {
            Toast.makeText(this, "please input goodsId", Toast.LENGTH_LONG).show();
            return;
        }

        SamplePaymentCallback paymentCallback = new SamplePaymentCallback();

        QGOrderInfo orderInfo = new QGOrderInfo();
        orderInfo.setOrderSubject("demo");
        orderInfo.setProductOrderId("replace with game orderId");
        orderInfo.setAmount(0.99);
        orderInfo.setSuggestCurrency("USD");
        orderInfo.setGoodsId(totalFeeStr);
        orderInfo.setExtrasParams("extra_paras");

        QGRoleInfo roleInfo = new QGRoleInfo();
        roleInfo.setRoleId("1001");
        roleInfo.setRoleLevel("10");
        roleInfo.setRoleName("replace with roleName");
        roleInfo.setServerName("replace with serverName");
        roleInfo.setVipLevel("14");
        sdkInstance.pay(this, orderInfo, roleInfo, paymentCallback);
    }

    //user bind info
    public void callUserBindInfo(View v) {
        QGUserBindInfo info = sdkInstance.getUserBindInfo();
        Log.i("quickgame","UserBindInfo: " + info.getEmailAccountName());
        Log.i("quickgame","UserBindInfo: isBindInfo" + info.isBindEmail());
        Toast.makeText(this, "UserBindInfo Email:" + info.getEmailAccountName(), Toast.LENGTH_LONG).show();
    }

    //user bind info
    public void callBindEmail(View v) {
        QGUserBindInfo info = sdkInstance.getUserBindInfo();
        if(info.isBindEmail()) {
            Toast.makeText(this, "Binded already. Email:" + info.getEmailAccountName(), Toast.LENGTH_LONG).show();
        } else {
            QuickGameManager.getInstance().setUserBindCallback(new QuickGameManager.QGUserBindCallback() {
                @Override
                public void onBindInfoChanged(String uid, boolean isBindUnBindSuccess,QGUserBindInfo qgUserBindInfo) {
                    //isBindUnBindSuccess ：Whether the binding or unbinding was successful
                    if (qgUserBindInfo.isBindEmail()) {
                        Log.d("mainActivity", "bind email");
                        Toast.makeText(MainActivity.this, "Just binded. Email:" + info.getEmailAccountName(), Toast.LENGTH_LONG).show();
                    } else {
                        Log.d("GameActivity", "unbind facebook");
                    }
                }

                @Override
                public void onexitUserCenter() {
                    Log.e("GameActivity", "Logout of the user center");
                }
            });

            QuickGameManager.getInstance().bindUser(QGConstant.LOGIN_OPEN_TYPE_EMAIL);
        }
    }

    //user info
    public void callUserInfo(View v) {
        QGUserData info = sdkInstance.getUser();
        info.getUserName();
        Log.i("quickgame","UserBindInfo.getUserName: " + info.getUserName());
        Log.i("quickgame","UserBindInfo.getdisplayUid: " + info.getdisplayUid());
        Log.i("quickgame","UserBindInfo.getUid: " + info.getUid());
        Log.i("quickgame","UserBindInfo.getFBUid: " + info.getFBUid());
        Log.i("quickgame","UserBindInfo.getGoogleUid: " + info.getGoogleUid());
        Log.i("quickgame","UserBindInfo.getOpenType: " + info.getOpenType());
        Log.i("quickgame","UserBindInfo.getToken: " + info.getToken());
        Toast.makeText(this, "userName:" + info.getUserName() + ", displayUid: " + info.getdisplayUid() + ", uid: " + info.getUid(), Toast.LENGTH_LONG).show();
    }

    //logout
    public void callLogout(View v) {
        sdkInstance.logout(this);
    }

    //claim quest
    public void claimQuest(View v) {
        noctuaSdk.openClaimQuest(this);
    }

    //redeem reward
    public void redeemReward(View v) {
        noctuaSdk.openRedeemReward(this, "test","test");
    }

    //redeem web
    public void openWeb(View v) {
        noctuaSdk.openURL(this, "https://noctua.gg/account/test-bind");
    }

    //arena win
    public void sendArenaWin(View v) {
        noctuaSdk.reportQuest("win_arena",1);
    }

    //login
    public void sendLoginEvent(View v) {
        noctuaSdk.reportQuest("login",1);
    }


    // usercenter
    public void callInAppReview(View v) {
        ReviewManager manager = ReviewManagerFactory.create(this);
        Task<ReviewInfo> request = manager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Toast.makeText(MainActivity.this, "Success to get review request", Toast .LENGTH_LONG).show();
                Log.i("quickgame", "Success to get review request");
                Log.i("quickgame", "Code is" + task.hashCode());
                // We can get the ReviewInfo object
                ReviewInfo reviewInfo = task.getResult();
                Task<Void> flow = manager.launchReviewFlow(this, reviewInfo);
                flow.addOnCompleteListener(task2 -> {
                    Toast.makeText(MainActivity.this, "Finish giving review", Toast .LENGTH_LONG).show();
                });
            } else {
                // There was some problem, log or handle the error code.
                Log.e("quickgame", task.getException().getMessage());
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
    }


    // usercenter
    public void callFacebookShare(View v) {
        Bitmap ss = takeScreenShot(this);
        sdkInstance.facebookShareBitmap(this, ss, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(MainActivity.this, "Facebook Share Success", Toast .LENGTH_LONG).show();
                Log.i("quickgame","Facebook Share Success");
            }

            @Override
            public void onCancel() {
                Toast.makeText(MainActivity.this, "Facebook Share Canceled", Toast .LENGTH_LONG).show();
                Log.i("quickgame","Facebook Share Canceled");
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(MainActivity.this, "Facebook Share Error", Toast .LENGTH_LONG).show();
                Log.e("quickgame","Facebook Share Error");
                error.printStackTrace();
            }
        });
    }


    // usercenter
    public void callUserCenter(View v) {
        sdkInstance.enterUserCenter(this);
    }

    //exit
    public void closeApp(View v) {
        finish();
    }

    private Bitmap takeScreenShot(Activity activity)
    {
        // create bitmap screen capture
        View v1 = getWindow().getDecorView().getRootView();
        v1.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());

        return bitmap;
    }

    /**
     * sdk login callback
     */
    private class SampleSDKCallback implements SDKCallback {

        @Override
        public void onLoginFinished(QGUserData userData, QGUserHolder loginState) {
            if (loginState.getStateCode() == QGUserHolder.LOGIN_SUCCESS) {
                SharedPreferences shared = MainActivity.this.getSharedPreferences("com.noctuagames.android.yggdrasil.", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = shared.edit();
                editor.putBoolean("has_logged_in", true);
                editor.apply();

                //show float view
                sdkInstance.showFloatView(MainActivity.this);
                MainActivity.this.findViewById(R.id.ll_content).setVisibility(View.VISIBLE);
                QGRoleInfo roleInfo = new QGRoleInfo();
                roleInfo.setRoleId("1001");
                roleInfo.setRoleLevel("10");
                roleInfo.setRoleName("replace with roleName");
                roleInfo.setServerName("replace with serverName");
                roleInfo.setVipLevel("14");
                roleInfo.setServerId("12345");
                //report role info
                sdkInstance.submitRoleInfo(userData.getUid(), roleInfo);

                Log.i("quickgame","Login successs");
                Log.i("quickgame","UserBindInfo.getUserName: " + userData.getUserName());
                Log.i("quickgame","UserBindInfo.getdisplayUid: " + userData.getdisplayUid());
                Log.i("quickgame","UserBindInfo.getUid: " + userData.getUid());
                Log.i("quickgame","UserBindInfo.getOpenType: " + userData.getOpenType());
                Toast.makeText(MainActivity.this, "loginSuccess. userName:" + userData.getUserName() + ", displayUid: " + userData.getdisplayUid() + ", userData: " + userData.getUid(), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(MainActivity.this, "login fail", Toast.LENGTH_LONG).show();
            }

        }

        @Override
        public void onInitFinished(boolean isSuccess) {
            if (isSuccess) {
                Toast.makeText(MainActivity.this, "channelId:" + sdkInstance.getChannelId() + "init success->", Toast .LENGTH_LONG).show();
                Log.i("quickgame","channelId:" + sdkInstance.getChannelId() + "init success->");
                loginBtn.setEnabled(true);
                QGLog.setDebugMod(true);

                SharedPreferences shared =  MainActivity.this.getSharedPreferences("com.noctuagames.android.yggdrasil.", Context.MODE_PRIVATE);
                boolean hasLoggedIn = shared.getBoolean("has_logged_in", false);
                if(hasLoggedIn) {
                    sdkInstance.login(MainActivity.this);
                } else {
                    sdkInstance.freeLogin(MainActivity.this);
                }
            } else {
                Toast.makeText(MainActivity.this, "init fail", Toast.LENGTH_LONG).show();
                Log.i("quickgame","init failed");
            }
        }

        @Override
        public void onLogout() {
            MainActivity.this.runOnUiThread(() -> {
                Toast.makeText(MainActivity.this, "logout", Toast.LENGTH_LONG).show();
                MainActivity.this.findViewById(R.id.ll_content).setVisibility(View.GONE);
                MainActivity.this.findViewById(R.id.login).setVisibility(View.VISIBLE);
            });
        }

        @Override
        public void onGooglePlaySub(String goodsId, String sdkOrder, boolean isAutoRenewing, boolean isAcknowledged) {
            Log.d(TAG, "goodsId=" + goodsId + "&&sdkOrder=" + sdkOrder);
        }
    }

    /**
     * pay callback
     */
    private class SamplePaymentCallback implements QGPaymentCallback {

        @Override
        public void onPaySuccess(String orderId, String orderNo, String goodsId, String extraParams) {
            MainActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(MainActivity.this, "pay success", Toast.LENGTH_LONG).show();
                }
            });
        }

        @Override
        public void onPayFailed(String orderId, String orderNo, String errorMessage) {
            MainActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(MainActivity.this, "pay fail:" + errorMessage, Toast.LENGTH_LONG).show();
                }
            });
        }

        @Override
        public void onPayCancel(String orderId, String orderNo, String errorMessage) {
            MainActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(MainActivity.this, "pay cancel", Toast.LENGTH_LONG).show();
                }
            });
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        sdkInstance.onStart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sdkInstance.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sdkInstance.onPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        sdkInstance.onStop(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sdkInstance.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult, requestCode: " + requestCode + ", resultCode: " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        sdkInstance.onActivityResult(requestCode, resultCode, data);
        noctuaSdk.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        sdkInstance.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}